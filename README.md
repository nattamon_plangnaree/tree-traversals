# Tree traversals
#Balanced BST (AVL tree)
https://www.cs.usfca.edu/~galles/visualization/AVLtree.html?fbclid=IwAR1C1-18LyzlIl3Yp1VexV0P4qB3Kj_l-4Fx1WKoa0R6PK_jb-O7o_82scc
#Balanced BST (Red-Black tree)
https://www.cs.usfca.edu/~galles/visualization/RedBlack.html?fbclid=IwAR0Ol1-faqxZ0rYWtHtYWR0APjuQ5P-lsRZTfmPAFmDEA1xYwDnViGJKF8U
